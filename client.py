from Crypto.Cipher import AES
from Crypto.Hash import SHA256
from Crypto.PublicKey import RSA
import pickle
from socket import *
#Client
# TCP Client Code

host = "127.0.0.1"            # Set the server address to variable host

port = 4446               # Sets the variable port to 4446

s = socket(AF_INET, SOCK_STREAM)      # Creates a socket
s.connect((host,port))          # Connect to server address

msg = s.recv(1024)            # Receives data upto 1024 bytes and stores in variables msg

iv = b'1234567890ZYXWVU'

try:

    # Asking for the secret key for the decryption process
    key = input("please enter the secret key to decrypt the message\n")

    decryption = AES.new(key.encode(),AES.MODE_CFB,iv)
    message = pickle.loads(msg)
    plain_txt = decryption.decrypt(message[0].strip()) #decrypting the message
    hash = SHA256.new()
    hash.update(plain_txt)
    hash_digest = hash.hexdigest()

    server_public_key_file = open("server_public_key.pem", 'r')
    server_public_key = RSA.importKey(server_public_key_file.read())

    if (server_public_key.verify(hash.digest(),message[2])):
        print ("message verified correctly, origin: server")
        print ("Message from server : " + plain_txt.decode('ascii'))

    else:
        print("There is a problem verifying the message, try again !!")

except:
    print("wrong key, try again !!")

s.close()                            # Closes the socket
# End of code
