from Crypto.Cipher import AES
from Crypto.PublicKey import RSA
from Crypto import Random
from Crypto.Hash import SHA256
import pickle
from socket import *
#Server
# TCP Server Code

host="127.0.0.1"                # Set the server address to variable host
#host="127.168.2.75"                # Set the server address to variable host
port=4446                   # Sets the variable port to 4446

s=socket(AF_INET, SOCK_STREAM)
s.bind((host,port))                 # Binds the socket. Note that the input to
s.listen(1)                         # Sets socket to listening state with a  queue
                                            # of 1 connection
print ("Listening for connections.. ")
q,addr=s.accept()               # Accepts incoming request from client and returns
                                            # socket and address to variables q and addr
data=input("Enter data to be send:  ")  # Storing the data as an input from user

# Generating RSA keys (Public key cryptography) private key should be
# used to sign the message and should provide authenticity as the message originates
random_generator = Random.new().read
keys_rsa = RSA.generate(1024, random_generator)
public_key_file = open("server_public_key.pem", "w")
public_key_file.write(keys_rsa.publickey().exportKey().decode())


# using SHA256 to generate a hash value of the message to ensure integrity
hash = SHA256.new()
hash.update(data.encode())
hash_digest = hash.hexdigest()
signature = keys_rsa.sign(hash.digest(),'')


key = b'ABCDEFGHIJ123456'
iv = b'1234567890ZYXWVU'

# encypting the message using AES before sending it to the client
encryption = AES.new(key,AES.MODE_CFB,iv)
cipher_txt = encryption.encrypt(data.encode())

print("encrypted data: \n", str(cipher_txt))

cipher_list=[cipher_txt,hash_digest,signature]
pickled_data= pickle.dumps(cipher_list)

q.send(pickled_data)                        # Sending data to client
s.close()
# End of code
